<%-- 
    Document   : edit
    Created on : 29/11/2017, 04:33:14 PM
    Author     : weyler
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar paciente</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    </head>
    <body>
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="<c:url value="home.htm"/>">Lista de pacientes</a></li>
                <li class="active">Editar</li>
            </ol>
            <div class="panel panel-primary">
                    <div class="panel-body">
                        
                        <form:form method="post" commandName="pacientes">
                            <h1>Completa el registro</h1>
                            
                            <p>
                                <form:label path="name">Nombre:</form:label>
                                <form:input path="name" cssClass="form-control"></form:input>
                            </p>
                            <p>
                                <form:label path="lastname">Apellido:</form:label>
                                <form:input path="lastname" cssClass="form-control"></form:input>
                            </p>
                            <p>
                                <form:label path="age">Edad:</form:label>
                                <form:input path="age" cssClass="form-control"></form:input>
                            </p>
                            <p>
                                <form:label path="warrant">Cédula:</form:label>
                                <form:input path="warrant" cssClass="form-control"></form:input>
                            </p>
                            <p>
                                <form:label path="city">Ciudad:</form:label>
                                <form:input path="city" cssClass="form-control"></form:input>
                            </p>
                            <p>
                                <form:label path="diagnostic">Diagnóstico:</form:label>
                                <form:input path="diagnostic" cssClass="form-control"></form:input>
                            </p>
                            <hr/>
                            <input type="submit" value="Enviar" class="btn btn-danger" />
                        </form:form>
                        
                    </div>
                </div>
        </div>
    </body>
</html>
