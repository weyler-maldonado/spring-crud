/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.ConnectionToDB;
import javax.servlet.http.HttpServletRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author weyler
 */
public class DeleteController {
    
    private JdbcTemplate jdbcTemplate;
    
    public DeleteController()
    {
        ConnectionToDB conn = new ConnectionToDB();
        this.jdbcTemplate = new JdbcTemplate(conn.conectar());
    }
    
    @RequestMapping("delete.htm")
    public ModelAndView home(HttpServletRequest request){
        
        int id = Integer.parseInt(request.getParameter("id"));
        this.jdbcTemplate.update("delete from patients " + "where " + "id=? ",id);
        
        return new ModelAndView("redirect:/home.htm");
        
    }
}
