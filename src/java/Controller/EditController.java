/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.ConnectionToDB;
import Model.Patient;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author weyler
 */
@Controller
@RequestMapping("edit.htm")
public class EditController {
    
    Patient patient;
    private JdbcTemplate jdbcTemplate;
    
    public EditController(){
        this.patient =  new Patient();
        ConnectionToDB conn =  new ConnectionToDB();
        this.jdbcTemplate = new JdbcTemplate(conn.conectar());
    }
    
    @RequestMapping(method =  RequestMethod.GET)
    public ModelAndView form(HttpServletRequest request)
    {
        ModelAndView mv = new ModelAndView();
        int id = Integer.parseInt(request.getParameter("id"));
        Patient datos =  this.querySelect(id);
        mv.setViewName("edit");
        mv.addObject("pacientes", new Patient(id,datos.getName(),datos.getLastname(),datos.getAge(),datos.getWarrant(),datos.getCity(),datos.getDiagnostic()));
              
        return mv;
    }
    
    @RequestMapping(method = RequestMethod.POST)
   public ModelAndView form
           (
             @ModelAttribute("patient") Patient p,
             BindingResult result,
             SessionStatus status,
             HttpServletRequest request
           )
           {
               
               int id = Integer.parseInt(request.getParameter("id"));
               this.jdbcTemplate.update(
               "update patients " + "set name = ?," + " lastname = ?," + " age = ?," + " warrant = ?," + " city = ?," + " diagnostic = ? " + " where " + "id = ?", 
                p.getName(), p.getLastname(), p.getAge(), p.getWarrant(), p.getCity(), p.getDiagnostic(), id);
               return new ModelAndView("redirect:/home.htm");
           }
    
                
    public Patient querySelect(int id)
    {
        final Patient patient = new Patient();
        String query = "SELECT * FROM patients WHERE id='"+id+"'";
        return (Patient) jdbcTemplate.query(query, new ResultSetExtractor<Patient>(){
  
            public Patient extractData(ResultSet rs) throws SQLException, DataAccessException 
            {
                if(rs.next())
                {
                    
                    patient.setName(rs.getString("name"));
                    patient.setLastname(rs.getString("lastname"));
                    patient.setAge(Integer.parseInt( rs.getString("Age") ));
                    patient.setWarrant(rs.getString("warrant"));
                    patient.setCity(rs.getString("city"));
                    patient.setDiagnostic(rs.getString("diagnostic"));
                }
                return patient;
            }
            
        });
    }
}
