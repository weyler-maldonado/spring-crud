/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.ConnectionToDB;
import Model.Patient;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author weyler
 */
@Controller
@RequestMapping("add.htm")
public class AddController {
    Patient patient;
    private JdbcTemplate jdbcTemplate;
    
   public AddController()
   {
       this.patient = new Patient();
       ConnectionToDB  conn =  new ConnectionToDB();
       this.jdbcTemplate =  new JdbcTemplate(conn.conectar());
   }
   
   @RequestMapping(method = RequestMethod.GET)
   public ModelAndView form()
   {
       ModelAndView mv =  new ModelAndView();
       mv.setViewName("add");
       mv.addObject("pacientes", new Patient());
       
       return mv;
       
   }
   
   @RequestMapping(method = RequestMethod.POST)
   public ModelAndView form
           (
             @ModelAttribute("patient") Patient p,
             BindingResult result,
             SessionStatus status
           )
           {
               this.jdbcTemplate.update(
               "insert into patients (name,lastname,age,warrant,city,diagnostic) values(?,?,?,?,?,?)", 
                p.getName(), p.getLastname(), p.getAge(), p.getWarrant(), p.getCity(), p.getDiagnostic()
               );
               return new ModelAndView("redirect:/home.htm");
           }
    
    
}
