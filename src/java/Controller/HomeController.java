/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Model.ConnectionToDB;
import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 *
 * @author weyler
 */


public class HomeController {
    
    private JdbcTemplate jdbcTemplate;
    
    public HomeController(){
        ConnectionToDB connection = new ConnectionToDB();
        this.jdbcTemplate =  new  JdbcTemplate(connection.conectar());
    }
    
    @RequestMapping("home.htm")
    public  ModelAndView home()
    {
        ModelAndView mv = new ModelAndView();
        String query = "select * from patients";
        List result = this.jdbcTemplate.queryForList(query);
        mv.addObject("datos",result);
        mv.setViewName("home");
        return mv;
    }
    
    
}
