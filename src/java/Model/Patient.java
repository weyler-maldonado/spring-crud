/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author weyler
 */
public class Patient 
{
    private int id;
    private String name;
    private String lastname;
    private int age;
    private String warrant;
    private String city;
    private String diagnostic;

    public Patient() {
    }

    public Patient(String name, String lastname, int age, String warrant, String city, String diagnostic) {
        this.name = name;
        this.lastname = lastname;
        this.age = age;
        this.warrant = warrant;
        this.city = city;
        this.diagnostic = diagnostic;
    }

    public Patient(int id, String name, String lastname, int age, String warrant, String city, String diagnostic) {
        this.id = id;
        this.name = name;
        this.lastname = lastname;
        this.age = age;
        this.warrant = warrant;
        this.city = city;
        this.diagnostic = diagnostic;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getWarrant() {
        return warrant;
    }

    public void setWarrant(String warrant) {
        this.warrant = warrant;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDiagnostic() {
        return diagnostic;
    }

    public void setDiagnostic(String diagnostic) {
        this.diagnostic = diagnostic;
    }
    
    
    
    
    
    
    
}
