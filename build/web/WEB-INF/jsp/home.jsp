<%-- 
    Document   : home
    Created on : 29/11/2017, 12:10:38 PM
    Author     : weyler
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta  content="charset=UTF-8">
        <title>Registro</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    </head>
    <body>
        <div class="container">  
            <div class="row">
                <h1>Registro</h1>
                <p>
                    <a href="<c:url value="add.htm"/>" class="btn btn-success"> <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Agregar </a>
                </p>
                
                <table class="table table-bordered tables-striped table-hover">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Edad</th>
                            <th>Cédula</th>
                            <th>Ciudad</th>
                            <th>Diagnóstico</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${datos}" var="dato">
                            <tr>
                                <td><c:out value="${dato.id}"></c:out></td>
                                <td><c:out value="${dato.name}"></c:out></td>
                                <td><c:out value="${dato.lastname}"></c:out></td>
                                <td><c:out value="${dato.age}"></c:out></td>
                                <td><c:out value="${dato.warrant}"></c:out></td>
                                <td><c:out value="${dato.city}"></c:out></td>
                                <td><c:out value="${dato.diagnostic}"></c:out></td>
                                <td>
                                <a href="<c:url value="edit.htm?id=${dato.id}"/>" ><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                                <a href="<c:url value="delete.htm?id=${dato.id}"/>"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>                                                                                 
                                </td>
                            </tr>
                            
                        </c:forEach>
                    </tbody>
                    
                </table>
            </div>   
        </div>
    </body>
</html>
