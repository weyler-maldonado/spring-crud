# Proyecto de tercer parcial de Arquitectura de Software
Aplicación CRUD usando Spring 4.0.1 + Bootstrap 3 + Postgresql

## Prerequisitos
A continuación se en lista lo necesario para correr la apliación:

- Postgresql

- Java EE (Java Web Aplications)

- Internet (Bootstrap CDN)
## Preparación
La aplicación depende de una base de datos Postgresql llamada* Test* con una tabla llamada *patients.  [*Aquí](https://drive.google.com/drive/folders/1xu-wUanPkxbA2P_amiNXVUgjjkwN4gsw?usp=sharing) puedes descargar el backup de la tabla. Sin embargo, se adjunta la query para realizar la tabla:
```sql
CREATE TABLE public.patients
(
  id integer NOT NULL DEFAULT nextval('patients_id_seq'::regclass),
  name text NOT NULL,
  lastname text NOT NULL,
  age integer NOT NULL,
  warrant text NOT NULL,
  city text NOT NULL,
  diagnostic text NOT NULL,
  CONSTRAINT patients_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.patients
  OWNER TO postgres;

```
> Puedes cambiar la base de datos o la configuración que desee en el archivo **ConnectionToDB.java**

> ***Nota: Si usas Chrome necesitas una extensión de Netbeans para levantar la aplicación.***
## 
## Código por documentar
A continuación se enlistan las archivos a documentar:

- /WebPages/WEB-INF/jsp/add.jsp

- /WebPages/WEB-INF/jsp/edit.jsp

- /WebPages/WEB-INF/jsp/home.jsp

- /WebPages/dispatcher-servlet.xml (Enfocarse en los props y beans de los controladores)

- /src/java/Controller/AddController.java

- /src/java/Controller/EditController.java

- /src/java/Controller/DeleteController.java

- /src/java/Controller/HomeController.java

- /src/java/Model/ConnectionToDB.java

- /src/java/Controller/Patient.java